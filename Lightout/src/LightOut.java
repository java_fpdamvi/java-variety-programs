import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Random;
public class LightOut {
	
	static Scanner sc = new Scanner(System.in);
	static Random r = new Random();
	
	public static void main(String[] args) {
		
		boolean win = false;
		String jname = sc.nextLine();
		int f = sc.nextInt();
		int count = 0;
		int [][] matriz = new int[f][f]; 
		creacioMatriu(matriz);
		while(!win) {
			mostrar(matriz);
			coordenades coor = eleccioCasella();
			cambioCasella(matriz,coor);
			win = comprovaWin(matriz);
			count++;
		}
		System.out.println("WIN "+jname+" en "+count+" movimientos.");
	}
	
	private static void mostrar(int[][] matriz) {
		for (int i=0;i<matriz.length;i++) {
			for (int k=0;k<matriz[0].length;k++) {
				System.out.print(matriz[i][k]);
			}
			System.out.println("");
		}
	}

	private static void creacioMatriu(int[][] matriz) {
		
		for (int i=0;i<matriz.length;i++) {
			for (int k=0;k<matriz[0].length;k++) {
				if (r.nextInt(1,6) == 1) {
					matriz[i][k] = 1;
				}else {
					matriz[i][k] = 0;
				}
				
			}
		}
	}

	private static coordenades eleccioCasella() {
		coordenades coor = new coordenades();
		StringBuilder sb = new StringBuilder(sc.nextLine());
		coor.x = Integer.parseInt(sb.substring(0, sb.indexOf(",")));
		coor.y = sb.charAt(sb.length()) - 49;
		return coor;
	}

	private static void cambioCasella(int[][] matriz, coordenades coor) {
		if (matriz[coor.y][coor.x] == 0) {
			matriz[coor.y][coor.x] = 1;
		}else {
			matriz[coor.y][coor.x] = 0;
		}
		if (!meSalgoI(coor.y,coor.x-1,matriz)) {
			if (matriz[coor.y][coor.x-1] == 0) {
				matriz[coor.y][coor.x-1] = 1;
			}else {
				matriz[coor.y][coor.x-1] = 0;
			}
		}
		if (!meSalgoI(coor.y,coor.x+1,matriz)) {
			if (matriz[coor.y][coor.x+1] == 0) {
				matriz[coor.y][coor.x+1] = 1;
			}else {
				matriz[coor.y][coor.x+1] = 0;
			}
		}
		if (!meSalgoI(coor.y-1,coor.x,matriz)) {
			if (matriz[coor.y-1][coor.x] == 0) {
				matriz[coor.y-1][coor.x] = 1;
			}else {
				matriz[coor.y-1][coor.x] = 0;
			}
		}
		if (!meSalgoI(coor.y+1,coor.x,matriz)) {
			if (matriz[coor.y+1][coor.x] == 0) {
				matriz[coor.y+1][coor.x] = 1;
			}else {
				matriz[coor.y+1][coor.x] = 0;
			}
		}
		
	}

	private static boolean comprovaWin(int[][] matriz) {
		boolean win = true;
		for (int i=0;i<matriz.length;i++) {
			for (int k=0;k<matriz[0].length;k++) {
				if (matriz[i][k] == 0) {
					win = false;
				}
			}
		}
		return win;
	}

	public static boolean meSalgoI(int ff, int cf, int[][] m) {
		if(ff<0 || cf<0 || ff>m.length-1 || cf>m[0].length-1 ) {
			return true;
		}else {
			return false;
		}
	}
}
