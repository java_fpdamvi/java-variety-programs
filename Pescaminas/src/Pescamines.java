import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Random;
public class Pescamines {
	
	static Scanner sc = new Scanner(System.in);
	static Random r = new Random();
	public static void main(String[] args) {
		opcio op = new opcio();
		coordenades coor = new coordenades();
		op.nmines = 1;
		op.nom = "nom";
		op.c = 4;
		op.f = 4;
		op.bucle = true;
		op.game = false;
		op.guanyadors = new ArrayList<String>();
		while(op.bucle) {
			op = menu(op);
			int[][] tcamp = new int[op.f][op.c];
			int[][] tmines = new int[op.f][op.c];
			crearCamp(tcamp);
			crearMines(op.nmines,tmines);
			boolean win = false;
			while(op.game) {
				verCamp(tcamp);
				coor = escogirCasella(tcamp);
				canviCamp(tcamp,tmines,coor.x,coor.y);
				if (tmines[coor.y][coor.x] == 1) {
					System.out.println("Has perdut.");
					campDescobert(tcamp,tmines,op.f,op.c);
					op.game = false;
				}else {
					win = comprovaWin(tcamp,op.nmines);
					if(win) {
						System.out.println("Has guanyat.");
						verCamp(tcamp);
						op.game = false;
						op.guanyadors.add(op.nom);
					}
					
				}
			}
		}
	}

	private static opcio menu(opcio op) {
		System.out.println("Menú: 1-Ajuda 2-Opcions 3-Jugar 4-Guanyadors 5-Sortir");
		System.out.print("Escull una opció: ");
		int num = sc.nextInt();
		switch(num) {
		case 1:
			System.out.println("Nom de jugador: "+op.nom);
			System.out.println("Número de files: "+op.f);
			System.out.println("Número de columnes: "+op.c);
			System.out.println("Número de mines: "+op.nmines);
			break;
		case 2:
			sc.nextLine();
			System.out.print("Nom de jugador: ");
			op.nom = sc.nextLine();
			System.out.print("Número de files: ");
			op.f = sc.nextInt();
			System.out.print("Número de columnes: ");
			op.c = sc.nextInt();
			System.out.print("Número de mines: ");
			op.nmines = sc.nextInt();
			break;
		case 3: 
			op.game = true;
			break;
		case 4:
			System.out.println(op.guanyadors);
			break;
		case 5:
			op.bucle = false;
			break;
		}
		return op;
	}

	private static void campDescobert(int[][] tcamp, int[][] tmines, int f, int c) {
		String [][] tfinal = new String[f][c];
		for(int i = 0;i<tcamp.length;i++) {
			for(int k=0;k<tcamp[0].length;k++) {
				if(tcamp[i][k] == 9) {
					if(tmines[i][k] != 1) {
						int count;
						count = cuentaBombas(tmines,k,i);
						tcamp[i][k] = count; 
					}
				}
				tfinal[i][k] = Integer.toString(tcamp[i][k]);
				if (tfinal[i][k].equals("9")) {
					tfinal[i][k] = "?";
				}
			}
		}
		for(int i = 0;i<tcamp.length;i++) {
			for(int k=0;k<tcamp[0].length;k++) {
				System.out.print(tfinal[i][k]+" ");
			}
			System.out.println("");
		}
		
	}

	private static boolean comprovaWin(int[][] tcamp, int nmines) {
		int count = 0;
		boolean win = false;
		for(int i = 0;i<tcamp.length;i++) {
			for(int k = 0;k<tcamp[0].length;k++) {
				if(tcamp[i][k] != 9) {
					count++;
				}
			}
		}
		if(tcamp.length*tcamp[0].length-count == nmines) {
			win = true;
		}
		return win;
	}

	private static coordenades escogirCasella(int[][] tcamp) {
		coordenades coor = new coordenades();
		boolean bucle = true;
		int x = 0;
		int y = 0;
		while (bucle) {
			x = sc.nextInt();
			y = sc.nextInt();
			if (tcamp[y][x] == 9) {
				coor.x = x;
				coor.y = y;
				bucle = false;
			}else {
				System.out.println("Torna a posar coordenades.");
			}
		}
		return coor;
	}

	private static void crearCamp(int[][] tcamp) {
		for(int i = 0;i<tcamp.length;i++) {
			for(int k = 0;k<tcamp[0].length;k++) {
				tcamp[i][k] = 9;
			}
		}
	}

	private static void verCamp(int[][] tcamp) {
		for(int i = 0;i<tcamp.length;i++) {
			for(int k = 0;k<tcamp[0].length;k++) {
				System.out.print(tcamp[i][k]+" ");
			}
			System.out.println("");
		}
		
	}

	private static void crearMines(int nmines,int[][] tmines) {
		for(int i=0;i<nmines;i++) {
			boolean bucle = true;
			while(bucle) {
				int nc = r.nextInt(0,tmines[0].length);
				int nf = r.nextInt(0,tmines.length);
				if (tmines[nf][nc] != 1) {
					bucle = false;
					tmines[nf][nc] = 1;
				}
			}
		}
	}
	private static void canviCamp(int[][] tcamp, int[][] tmines, int x, int y) {
		if(tmines[y][x] == 0 && tcamp[y][x]==9) {
			int count;
			count = cuentaBombas(tmines,x,y);
			tcamp[y][x] = count;
			if(tcamp[y][x] == 0){
				if(!meSalgoI(y-1,x,tcamp)) {
					canviCamp(tcamp,tmines,x,y-1);
				}
				if(!meSalgoI(y+1,x,tcamp)) {
					canviCamp(tcamp,tmines,x,y+1);
				}
				if(!meSalgoI(y,x-1,tcamp)) {
					canviCamp(tcamp,tmines,x-1,y);
				}
				if(!meSalgoI(y,x+1,tcamp)) {
					canviCamp(tcamp,tmines,x+1,y);
				}		
			}
		}
		
	}


	private static int cuentaBombas(int[][] tmines, int x, int y) {
		int count = 0;
		if(!meSalgoI(y-1,x,tmines)) {
			if(tmines[y-1][x] == 1) {
				count++;
			}
		}
		if(!meSalgoI(y+1,x,tmines)) {
			if(tmines[y+1][x] == 1) {
				count++;
			}
		}
		if(!meSalgoI(y,x-1,tmines)) {
			if(tmines[y][x-1] == 1) {
				count++;
			}
		}
		if(!meSalgoI(y,x+1,tmines)) {
			if(tmines[y][x+1] == 1) {
				count++;
			}
		}
		if(!meSalgoI(y+1,x+1,tmines)) {
			if(tmines[y+1][x+1] == 1) {
				count++;
			}
		}
		if(!meSalgoI(y-1,x-1,tmines)) {
			if(tmines[y-1][x-1] == 1) {
				count++;
			}
		}
		if(!meSalgoI(y+1,x-1,tmines)) {
			if(tmines[y+1][x-1] == 1) {
				count++;
			}
		}
		if(!meSalgoI(y-1,x+1,tmines)) {
			if(tmines[y-1][x+1] == 1) {
				count++;
			}
		}
		return count;
	}

	public static boolean meSalgoI(int ff, int cf, int[][] m) {
		if(ff<0 || cf<0 || ff>m.length-1 || cf>m[0].length-1 ) {
			return true;
		}else {
			return false;
		}
	}
}
