import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Random;
public class Memory {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		jugador j1 = new jugador();
		jugador j2 = new jugador();
		int n = 2;
		int m = 2;
		int cartesTotal = n*m;
		System.out.println("Nombre Jugador 1:");
		j1.nom = sc.nextLine();
		System.out.println("Nombre Jugador 2:");
		j2.nom = sc.nextLine();
		boolean bool = true;
		boolean bucle = false;
		boolean juego = true;
		while (juego) {
			bool = true;
			while (bool) {
				System.out.println("Escoge opción del menú:");
				System.out.println("1.- Jugar 2.- Configuració 3.- Salir");
				int eleccio = sc.nextInt();
				sc.nextLine();
				switch (eleccio){
					case 1:
						bucle = true;
						bool = false;
						break;
					case 2:
						System.out.println("Nombre Jugador 1:");
						j1.nom = sc.nextLine();
						System.out.println("Nombre Jugador 2:");
						j2.nom = sc.nextLine();	
						System.out.println("Número de filas:");
						n = sc.nextInt();
						System.out.println("Número de columnas:");
						m = sc.nextInt();
						cartesTotal = n*m;
						break;
					case 3:
						bucle = false;
						bool = false;
						juego = false;
						break;
					default:
						System.out.println("¡Escoge una opción correcta!");
						break;
				}
			}
			while (bucle) {
				boolean torn = true;
				int [][] publica = new int [n][m];
				int [][] secreta = new int [n][m];
				j1.nPunts = 0;
				j2.nPunts = 0;
				posarCartes(publica,secreta,cartesTotal);
				while(cartesTotal>0) {
					System.out.println("Puntos "+j1.nom+": "+j1.nPunts);
					System.out.println("Puntos "+j2.nom+": "+j2.nPunts);
					coordenades coor1 = escollirCarta(publica,secreta,j1,j2,torn);
					mostrar(publica);
					coordenades coor2 = escollirCarta(publica,secreta,j1,j2,torn);
					mostrar(publica);
					boolean igual = comparar(coor1,coor2,secreta,publica);
					if (igual) {
						cartesTotal -= 2;
						if(torn) {
							j1.nPunts++;
						}else {
							j2.nPunts++;
						}
					}
					torn = canviTorn(torn,igual);
				}
				calcularGuanyador(j1,j2);
				bucle = false;
			}
		}
	}

	private static void calcularGuanyador(jugador j1, jugador j2) {
		if (j1.nPunts == j2.nPunts) {
			System.out.println("EMPATE");
		}else if (j1.nPunts > j2.nPunts) {
			System.out.println("GANA "+j1.nom);
			j1.nVictories++;
		}else {
			System.out.println("GANA "+j2.nom);
			j2.nVictories++;
		}
		System.out.println("VICTORIAS "+j1.nom+": "+j1.nVictories);
		System.out.println("VICTORIAS "+j2.nom+": "+j2.nVictories);
		
	}

	private static boolean canviTorn(boolean torn, boolean igual) {
		if (igual && torn) {
			return true;
		}else if (igual && torn == false) {
			return false;
		}else if (torn) {
			return false;
		}else {
			return true;
		}
		
	}

	private static boolean comparar(coordenades coor1, coordenades coor2, int[][] secreta,int[][] publica) {
		if(secreta[coor1.y][coor1.x] == secreta[coor2.y][coor2.x]) {
			return true;
		}else {
			publica[coor1.y][coor1.x] = 0;
			publica[coor2.y][coor2.x] = 0;
			return false;
		}
	}

	private static coordenades escollirCarta(int[][] publica, int[][] secreta, jugador j1, jugador j2, boolean torn) {
		Scanner sc = new Scanner(System.in);
		coordenades coor = new coordenades();
		if (torn) {
			System.out.println(j1.nom+", escoge carta.");
		}else {
			System.out.println(j2.nom+", escoge carta.");
		}
		boolean salir = true;
		while (salir) {
			System.out.println("Coordenada X:");
			coor.x = sc.nextInt();
			System.out.println("Coordenada Y:");
			coor.y = sc.nextInt();
			if (publica[coor.y][coor.x] == 0) {
				salir = false;
			}else {
				System.out.println("Vuelve a seleccionar posición:");
			}
		}
		sc.nextLine();
		publica[coor.y][coor.x] = secreta[coor.y][coor.x];
		return coor;
	}

	private static void posarCartes(int[][] publica, int[][] secreta, int total) {
		Random r = new Random();
		for (int i=0;i<publica.length;i++) {
			for (int k=0;k<publica[0].length;k++) {
				publica[i][k] = 0;
			}
		}
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i=1;i<=total/2;i++) {
			list.add(i);
			list.add(i);
		}
		for (int i=0;i<publica.length;i++) {
			for (int k=0;k<publica[0].length;k++) {
				int num = r.nextInt(0,total);
				secreta[i][k] = list.get(num);
				list.remove(num);
				total--;
			}
		}
	}

	private static void mostrar(int[][] publica) {
		for (int i=0;i<publica.length;i++) {
			for (int k=0;k<publica[0].length;k++) {
				System.out.print(publica[i][k]);
			}
			System.out.println("");
		}
	}
		
}



	

	
